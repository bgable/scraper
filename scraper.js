/**
 * Ben Gable
 * HTML Scraper REST API
 * Built for MassDrop coding challenge
 */

//Express web routing framework
const express = require('express');
const app = express();

//Request for making HTTP requests and getting HTML
var request = require('request');

//MySQL nodeJS plugin
var mysql = require('mysql');

//Simple queue package for queueing workers
var Queue = require('better-queue');
var scrapeQ = new Queue(scrape, {
  batchSize: 50,
  batchDelay: 5000,
  batchDelayTimeout: 1000,
  maxRetries: 10,
  retryDelay: 1000
});
var currentJobID = 0;

var sqlQ = new Queue(writeToDB, {
  batchSize: 10,
  batchDelay: 5000,
  batchDelayTimeout: 1000,
  maxRetries: 10,
  retryDelay: 1000
});

//URL validation
var validUrl = require('valid-url');

//DB ERROR CODES
const URL_INVALID = "urlinvalid";

/**
 * Server startup
 * Listens on port 3000
 */
app.listen(3000, function () {
  console.log('bgable WebScraper ONLINE Port:3000');
  
  //Get number of jobs in DB to seed the jobID
  //Could be done in a much more intelligent way, but for this simple app it works
  var query = "SELECT COUNT (*) FROM jobs";
  doSQL(query, (error, result) => {
    currentJobID = result[0]["COUNT (*)"];
  });
});

/**
 * Default landing page
 * Sends index.html w/ REST instructions
 */
app.get('/', function (req, res) {
  var options = {
    root: __dirname + '/public/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };      
  res.sendFile('index.html', options, function (err) {
    if (err) {
      throw err;
    }
  });
});

/**
 * getjob
 * GET
 * @param jobId - The id of the job to fetch
 * @param preview - If 0, will send back JSON, otherwise will attempt to display in browser
 * Returns the HTML of the job, or if not finished, an error code
 */
app.get('/getjob/:jobID/', function (req, res) {
  sendJob(req, res, false);
});

app.get('/getjob/:jobID/:preview', function (req, res) {
  sendJob(req, res, req.params.preview == "1");
});

//Informative return for missing jobID
app.get('/getjob/', function (req, res) {
  res.json(new ScrapeResponse(-1, 0, "Please provide a jobID."));
});

/**
 * sendJob
 * Main worker function for getjob
 * @param {*} req - request from express
 * @param {*} res - response object from express
 * @param {*} isPreview - whether or not this is a preview request
 */
function sendJob(req, res, isPreview = false) {
  var options = {
      root: __dirname + '/public/',
      dotfiles: 'deny',
      headers: {
          'x-timestamp': Date.now(),
          'x-sent': true
      }
    };

    //Check for valid job ID
    var jobID = req.params.jobID;
    if (isNaN(jobID)) {
      res.json(new ScrapeResponse(jobID, 0, "The job ID is not valid."));
    } else {
      doSQL("SELECT file FROM scraper.jobs WHERE id = " + jobID, (error, result) => {  
        if (error) {
          throw error;
        }

        //Job is finished
        if (result.length > 0) {
          
          //We found an entry, check if file is null (not finished)
          let fileData = Buffer.from(result[0].file, 'base64').toString('utf8');
          switch (fileData) {
            case URL_INVALID:
              res.json(new ScrapeResponse(jobID, 0, "The URL was invalid."));  
              break;
            default:
              if (isPreview) {  
                res.send(fileData, options, function (err) {
                  if (err) throw err;
                });
              } else {
                res.json(new ScrapeResponse(jobID, 1, "", fileData));
              }
          }

        //No entry found Job not finished or DNE
        } else {
          res.json(new ScrapeResponse(jobID, 0, "The job ID was not found or is not finished."));
        }
      });
    }
}

/**
 * scrape
 * POST
 * @param url - the URL to scrape
 * Takes a URL and adds a worker to the queue to scrape
 */
app.post('/scrape/*', function (req, res) {
  var jobID = currentJobID;
  var url = 'http://' + req.params[0];

  console.log(validUrl.isWebUri(url));
  if (validUrl.isWebUri(url)) {
    res.json(new ScrapeResponse(jobID));
    scrapeQ.push(new ScrapeData(url, jobID))
    .on('failed', (error) => {
      //If the request failed, write the error to the DB
      sqlQ.push({
        "taskId": jobID, 
        "file": URL_INVALID
      });
    });
    currentJobID++; 
  } else {
    res.json(new ScrapeResponse(jobID, 0, "URL is not valid"));
  }
});

//Informative return for missing URL
app.post('/scrape', function (req, res) {
  res.json(new ScrapeResponse(-1, 0, "URL is missing"));
});

//Informative return for invalid GET
app.get('/scrape/*', function (req, res) {
  res.json(new ScrapeResponse(-1, 0, "GET request not supported, please use POST."));
});

/**
 * scrape
 * Scrapes HTML using request. Called from worker queue
 * @param {url, id} workerData - Information about the task
 * @param {*} cb - Callback for worker queue
 */
function scrape(workerData, cb) {
  for (var i = 0; i < workerData.length; i++) {
    
    var url = workerData[i].url;
    var id = workerData[i].id;
    var mycb = cb;
    console.log("Scrape worker for " + url);
    console.log("ID is " + id);

    //Get HTML and send to be written to the DB
    request(url, requestCallback.bind({myid: id}));
  }

  console.log("callback");
  cb();
}

function requestCallback(err, response, body) {
  if (err) {
    console.log("error");
    sqlQ.push({
        "taskId": this.myid, 
        "file": URL_INVALID
      });
  } else {
    console.log("writing to DB");
    console.log("id is " + this.myid);
    var data = {
      "taskId": this.myid, 
      "file": body
    };
    sqlQ.push(new SQLData(this.myid, body));
  }
}

/**
 * writeToDB
 * Writes the file as a MEDIUMBLOB in the DB indexed by taskID
 * @param {taskId, file} result - The object containing the task ID and HTML to save in DB
 */
function writeToDB(writes, cb) {
  var inserts = [];
  var query = "REPLACE INTO jobs (id, file) VALUES";

  for (var i = 0; i < writes.length; i++) {
    var file = writes[i].file;
    var id = writes[i].taskId;

    //Create a buffer from the HTML string and save in BLOB format
    var buffer = Buffer.from(file);
    query = query.concat(" ( ? , ? )");
    if (i < writes.length-1) query = query.concat(",");
    inserts.push(id);
    inserts.push(buffer);
  }
  //console.log(query)
  //console.log(inserts)
  var final = mysql.format(query, inserts);
  //console.log(final)
  doSQL(final);

  cb();
}

/**
 * doSQL
 * Handles SQL queries and returns the result
 * @param {*} query - The SQL query to run
 * @param {*} callBack - Callback to run after query has finished (w/ resulting data)
 */
function doSQL(query, callBack) {
  
  /**
   * NOTE - Storing a plaintext password (and potentially DB host) is not secure however
   * this file is never served to the client. But if someone had
   * access to the server they could potentially download the source.
   * For this coding test, its OK, but for production we'd want
   * to store this in a protected file somewhere else with restricted
   * permissions.
   */
  
  var con = mysql.createConnection({
    host: "bgabledev.cva3ebmjfnom.us-west-1.rds.amazonaws.com",
    user: "bgable",
    password: "massdrop",
    database: "scraper"
  });

  con.connect(function(err) {
    if (err) throw err;
    con.query(query, callBack);
  });
}

/**
 * ScrapeResponse
 * The generic response object sent to the client after requests
 * @param {*} jobID - The jobID this response is adressed to
 * @param {*} result - The result: 1 if successful, 0 if not
 * @param {*} errorMessage - A message containing the error
 */
function ScrapeResponse (jobID, result = 1, errorMessage = null, data = null) {
  this.jobID = jobID;
  this.result = result;
  this.errorMessage = errorMessage;
  this.data = data;
}

function ScrapeData(url, id) {
  this.url = url;
  this.id = id;
}

function SQLData(taskId, file) {
  this.taskId = taskId;
  this.file = file;
}